

// stop from from being submitted on the front end

$('form').submit(function (e) {
    var error = "";

    // check and prompt if user filled in email correctly
    if($("#inputEmail4").val() =="") {

        error += "<p>The email field is required.</p>";
    }
    // check and prompt if user filled in password correctly
    if($("#inputPassword4").val() =="") {

        error += "<p>The password field is required.</p>";
    }
    // check and prompt if user filled in inputAddress correctly
    if($("#inputAddress").val() =="") {

        error += "<p>The address field is required.</p>";
    }
    // check and prompt if user filled in city correctly
    if($("#inputCity").val() =="") {

        error += "<p>The city field is required.</p>";
    }
    // check and prompt if user filled in city correctly
    if($("#inputState").val() =="") {

        error += "<p>The state field is required.</p>";
    }
    // check and prompt if user filled in city correctly
    if($("#inputZip").val() =="") {

        error += "<p>The zip field is required.</p>";
    }

    if(error !="") {
        $("#error").html('<div class="alert alert-danger" role="alert"><p><strong>There were error(s) in your form:</strong></p>' + error + '</div>');
    }
});